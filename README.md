# opencms-deploy

Deploy opencms on a 3 node setup - web,db and app server on centos

**Tested on:**
```
Centos 7.3
Apache 2.4
Tomcat 7
Mysql 5.7
OpenCMS 10.5
```


**VirtualBox Setup:**
```
Vbox - 6.x
Nw - Nat Network
```
